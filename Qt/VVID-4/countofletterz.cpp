#include "funcs.h"

/**
    Функция, считающая количество латинских букв "Z" в строке
    @param strIn исодная строка
    @return колчество латинских букв "Z" в данной строке
*/
int countOfLetterZ(QString strIn)
{
    strIn = strIn.toLower();
    int count = 0;

    for(int i = 0; i < strIn.length(); i++)
    {
        if(strIn[i] == 'z') count++;
    }

    return count;
}
