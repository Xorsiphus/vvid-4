#include "funcs.h"

/**
    Функция, считающая количество букв в строке
    @param strIn исходная строка
    @return количество букв в данной строке
*/
int countLetters(QString strIn)
{
    strIn = strIn.toLower();
    int count = 0;

    for(int i = 0; i < strIn.length(); i++)
    {
        if(strIn[i] < "а" || strIn[i] > "я")
        {
            strIn.remove(i, 1);
            i--;
        }
    }

    count = strIn.length();

    return count;
}
