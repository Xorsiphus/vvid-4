#include "funcs.h"

/**
    Функция, считающая количество латинских букв "А" в строке
    @param strIn исодная строка
    @return колчество латинских букв "А" в данной строке
*/
int countOfLetterA(QString strIn)
{
    strIn = strIn.toLower();
    int count = 0;

    for(int i = 0; i < strIn.length(); i++)
    {
        if(strIn[i] == 'a') count++;
    }

    return count;
}
